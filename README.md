## Elm SSR Exploration

A little experiment trying to implement server side rendering using Elm 0.19 and ports.

### Try it!

Just run the typical `npm` commands:

```bash
$ npm install
$ npm run build
$ npm run start
```

Open [`http://localhost:8125/`](http://localhost:8125/) in your favorite web browser - you should see the [Cat GIFs example](https://elm-lang.org/examples/cat-gifs), but the initial HTML and model are pre-rendered on the server side and injected into the HTML! You can verify this by disabling Javascript, or by looking at the page source using `CTRL-U` or `curl`.

### How does it work?

The main idea is to be able to decode and encode the `Html Msg` and `Model` values to be able to pass them back to Node to send to the browser, as well as decode the `Model` there, and pass it in as a flag.

All normal application code is in `src/Main.elm`, the shared source file for both the client and the server side.


Since we can't automatically generate Decoder/encode tuples, we have to provide those in our application. For the HTML, we use `zwilias/elm-html-string` intead of `elm/html`, giving us the ability to turn our view either into normal `Html` values, or into Strings, which we can send to Javascript as well.

The app also has to provide a `finishedLoading : Msg` value for that. As soon as this `Msg` is recieved by the `update` function, it does a final update and render, before sending the model and the HTML over to the Node.js server.

On the client side, it also expects a `hydrate` function, which gets called in place of `init` to start the application with a pre-existing model.

The SSR special sauce is happening in `src/SSR.elm`: It defines 2 new `Program` constructors: `server` and `client`, which both resemble the API of `Browser.element` with the additions mentioned above. They basically wrap the "normal" Elm app in `Main` in 2 different ways.

### What about native support?

There is a LOT of ceremony involved into getting this to work.

 * **It uses a different HTML library to be able to "stringify" the HTML** \
    Good luck elm-ui users!

 * **Models need to be `Decoder`-able** \
      A `Decoder Model` as well as a `Model -> Value` function need to be hand-written or generated at compile time. The argument for doing it that way was (from my understanding) that it forces you to validate your data explicitely, which avoids getting unexpected values into Elm programs. Here, we use it to just "round-trip" values from Elm to Elm, where both apps are _guaranteed_ to share the same Model.

 * **Hydration could be improved** \
      With my current implementation, instead of doing "real" hydration, the Elm runtime basically throws the static HTML we sent away, just to render the same HTML again.

 * **Weird `FinishedLoading` message** \
      In a native version, this could be a `Cmd msg` instead.

 * **Node.js and Browsers are very different environments** \
     Even to support simple XHR calls, I had to install an additional "polyfill" to patch the Node `global` object with an API-compatible version of `XMLHttpRequest`. There are probably even more problems if you use `elm/browser` APIs.

     A broader question would be what should happen in those cases. How would Elm support multiple target platforms? The server provides different APIs from the Browser, and an (imaginary) "native" target would support different effects as well.

     Is using `elm/browser` in a different environment a compiler error? This would probably require a way to conditionally compile code for different environments. Do we throw commands for effect managers that are not supported away? Is it a runtime error? How would you check that this is happening?

