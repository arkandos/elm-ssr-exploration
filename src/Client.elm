module Client exposing (main)

import Main exposing (Model, Msg)
import SSR exposing (ClientProgram)


main : ClientProgram Model Msg
main =
    SSR.client
        { init = Main.init
        , update = Main.update
        , subscriptions = Main.subscriptions
        , view = Main.view
        , decoder = Main.decoder
        , hydrate = Main.hydrate
        }
