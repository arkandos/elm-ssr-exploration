module Main exposing (Model, Msg, decoder, encode, init, hydrate, finishedLoading, update, subscriptions, view)

import Html.String exposing (..)
import Html.String.Attributes exposing (..)
import Html.String.Events exposing (..)
import Http
import Json.Decode as Json exposing (Decoder)
import Json.Encode as Encode
import SSR exposing (send)


type Model
    = Failure
    | Loading
    | Success String


decoder : Decoder Model
decoder =
    Json.andThen
        (\tag ->
            case tag of
                "Failure" ->
                    Json.succeed Failure

                "Loading" ->
                    Json.succeed Loading

                "Success" ->
                    Json.field "url" Json.string
                        |> Json.map Success

                _ ->
                    Json.fail "Unknown Tag"
        )
        (Json.field "$" Json.string)


encode : Model -> Json.Value
encode model =
    case model of
        Failure ->
            Encode.object [ ( "$", Encode.string "Failure" ) ]

        Loading ->
            Encode.object [ ( "$", Encode.string "Loading" ) ]

        Success url ->
            Encode.object
                [ ( "$", Encode.string "Success" )
                , ( "url", Encode.string url )
                ]


type Msg
    = FinishedLoading
    | MorePlease
    | GotGif (Result Http.Error String)


init : Json.Value -> ( Model, Cmd Msg )
init _ =
    ( Loading, getRandomCatGif )


hydrate : Json.Value -> Model -> ( Model, Cmd Msg )
hydrate _ model =
    ( model, Cmd.none )


finishedLoading : Msg
finishedLoading =
    FinishedLoading


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        FinishedLoading ->
            ( model, Cmd.none )

        MorePlease ->
            ( Loading, getRandomCatGif )

        GotGif (Ok url) ->
            ( Success url, SSR.send FinishedLoading )

        GotGif (Err err) ->
            let
                _ = Debug.log "ERROR" err
            in
            ( Failure, send FinishedLoading )


subscriptions : Model -> Sub Msg
subscriptions =
    always Sub.none


view : Model -> Html Msg
view model =
    div []
        [ h2 [] [ text "Random Cats" ]
        , case model of
            Failure ->
                div []
                    [ text "I could not load a random cat for some reason :("
                    , button [ onClick MorePlease ] [ text "Try again!" ]
                    ]

            Loading ->
                text "Loading awesomeness..."

            Success url ->
                div []
                    [ img [ style "display" "block", src url ] []
                    , button [ onClick MorePlease ] [ text "More Please!" ]
                    ]
        ]


getRandomCatGif : Cmd Msg
getRandomCatGif =
    Http.get
        { url = "https://api.giphy.com/v1/gifs/random?api_key=dc6zaTOxFJmzC&tag=cat"
        , expect = Http.expectJson GotGif gifDecoder
        }


gifDecoder : Decoder String
gifDecoder =
    Json.at [ "data", "image_url" ] Json.string
