module SSR exposing (ClientProgram, ServerProgram, client, send, server)

import Browser
import Html.String as Html exposing (Html)
import Json.Decode as Json exposing (Decoder, Value)
import Json.Encode as Encode
import Task


type alias ClientProgram model msg =
    Program Value model msg


type alias ServerProgram model msg =
    Program Value model msg


send : msg -> Cmd msg
send msg =
    Task.succeed msg
        |> Task.perform identity


server :
    { init : Value -> ( model, Cmd msg )
    , update : msg -> model -> ( model, Cmd msg )
    , view : model -> Html msg
    , subscriptions : model -> Sub msg
    , encode : model -> Value
    , finishedLoading : msg
    , sendToNode : Value -> Cmd msg
    }
    -> ServerProgram model msg
server config =
    Platform.worker
        { init = config.init
        , subscriptions = config.subscriptions
        , update =
            \msg model ->
                if msg == config.finishedLoading then
                    let
                        ( newModel, cmd ) =
                            config.update msg model
                    in
                    ( newModel
                    , Cmd.batch
                        [ cmd
                        , Encode.object
                            [ ( "model", config.encode newModel )
                            , ( "view", Encode.string <| Html.toString 0 <| config.view newModel )
                            ]
                            |> config.sendToNode
                        ]
                    )

                else
                    config.update msg model
        }


type alias Flags model =
    { flags : Value
    , model : model
    }


flagsDecoder : Decoder model -> Decoder (Flags model)
flagsDecoder modelDecoder =
    Json.map2 Flags
        (Json.field "flags" Json.value)
        (Json.field "model" modelDecoder)


client :
    { init : Value -> ( model, Cmd msg )
    , hydrate : Value -> model -> ( model, Cmd msg )
    , update : msg -> model -> ( model, Cmd msg )
    , view : model -> Html msg
    , subscriptions : model -> Sub msg
    , decoder : Decoder model
    }
    -> ClientProgram model msg
client config =
    Browser.element
        { init =
            \flags ->
                case Json.decodeValue (flagsDecoder config.decoder) flags of
                    Ok ssrFlags ->
                        config.hydrate ssrFlags.flags ssrFlags.model

                    Err _ ->
                        config.init flags
        , update = config.update
        , view = config.view >> Html.toHtml
        , subscriptions = config.subscriptions
        }
