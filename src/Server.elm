port module Server exposing (main)

import Json.Decode exposing (Value)
import Main exposing (Model, Msg)
import SSR exposing (ServerProgram)


port sendToNode : Value -> Cmd msg


main : ServerProgram Model Msg
main =
    SSR.server
        { init = Main.init
        , update = Main.update
        , subscriptions = Main.subscriptions
        , encode = Main.encode
        , sendToNode = sendToNode
        , view = Main.view
        , finishedLoading = Main.finishedLoading
        }
