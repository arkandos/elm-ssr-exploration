const http = require('http')
const fs = require('fs')
const path = require('path')

const { Elm } = require('../build/server.js')

// polyfill XMLHttpRequest for elm/http
global.XMLHttpRequest = require('xhr2')


const port = process.env.PORT || 8125


const server = http.createServer(function (req, res) {
    if (req.url === '/' || req.url === '/index.html') {
        // on index, render the server-side Elm app.
        ssr(res)
    } else if (req.url === '/client.js') {
        // script requesting the client-side Elm app.
        fs.readFile(path.join(__dirname, '../build/client.js'), function (error, content) {
            if (error) {
                res.writeHead(404)
                res.end(error.message)
            } else {
                res.writeHead(200, { 'Content-Type': 'application/javascript' })
                res.end(content, 'utf-8')
            }
        })
    } else {
        res.writeHead(404)
        res.end()
    }
})


server.listen(port, function () {
    console.log("Server started - Listening on port", port)
})


function ssr (res) {
    const app = Elm.Server.init({})
    app.ports.sendToNode.subscribe(function ({ model, view }) {
        const html =
            `<!DOCTYPE html>
            <html>
            <head>
                <meta charset="utf-8">
                <title>Elm SSR Exploration</title>
                <script src="/client.js"></script>
            </head>
            <body>
                <div id="app">${view}</div>
                <script>
                    var model = (${JSON.stringify(model)});
                    var app = Elm.Client.init({
                        node: document.getElementById("app"),
                        flags: {
                            flags: null,
                            model: model
                        }
                    });
                </script>
            </body>
            </html>
            `

        res.writeHead(200);
        res.end(html);
    })
}

